/* Alumno: Alejandro Olid
   Descripción: Ejercicios metodos, parametros y tests.
   Fecha: 20/01/2022 */

using ExercicisOptimitzacio_AlejandroOlid;
using NUnit.Framework;
namespace TestProject1
{
    public class TestIsLeapYear
    {
        [Test]
        public void IsLeapYear_2022()
        {
            var year = 2022;
            ExercicisOptimitzacio.IsLeapYear(year);
        }
        
        [Test]
        public void IsLeapYear_1000()
        {
            var year = 1000;
            ExercicisOptimitzacio.IsLeapYear(year);
        }
        
        [Test]
        public void IsLeapYear_1600()
        {
            var year = 1600;
            ExercicisOptimitzacio.IsLeapYear(year);
        }
        
        [Test]
        public void IsLeapYear_negative()
        {
            var year = -2000;
            ExercicisOptimitzacio.IsLeapYear(year);
        }
        
        [Test]
        public void IsLeapYear_Null()
        {
            ExercicisOptimitzacio.IsLeapYear(null);
        }
    }
}